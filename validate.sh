#!/bin/bash

# validates all xml files referenced in countries.xml
xmllint --noout --valid xml/countries.xml

# checks countries xml files against dtd's
xmllint --noout --dtdvalid scheme/country.dtd xml/as.xml
xmllint --noout --dtdvalid scheme/country.dtd xml/ca.xml
xmllint --noout --dtdvalid scheme/country.dtd xml/jp.xml
xmllint --noout --dtdvalid scheme/country.dtd xml/nz.xml

# checks countries' xml files against relaxng definitions
xmllint --noout --relaxng scheme/country.rng xml/as.xml
xmllint --noout --relaxng scheme/country.rng xml/ca.xml
xmllint --noout --relaxng scheme/country.rng xml/jp.xml
xmllint --noout --relaxng scheme/country.rng xml/nz.xml