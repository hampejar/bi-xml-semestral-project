<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <xsl:apply-templates select="countries" mode="index"/>
    </xsl:template>

    <xsl:template match="/countries" mode="index">
        <html lang="en">
            <head>
                <meta charset="utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <title>
                    Welcome to CountryList!
                </title>
                <link rel="stylesheet" href="{resolve-uri('style/style.css')}"/>
            </head>
            <body>
                <div id="index-top">
                    <div>
                        <h1>Selected Countries List</h1>
                        <div>semestral project by Jaroslav Hampejs</div>
                    </div>
                    <div id="world-map">
                        <img src="https://www.cia.gov/library/publications/the-world-factbook/attachments/images/large/world_phy.jpg?1558019809"/>
                    </div>
                </div>
                <div id="countries-list">
                    <ul>
                        <xsl:apply-templates select="country"/>
                    </ul>
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="/countries/country">
        <li>
            <a href="{lower-case(@iso-code)}.html">
                <xsl:value-of select="header/country_name"/>
            </a>
        </li>
    </xsl:template>
</xsl:stylesheet>