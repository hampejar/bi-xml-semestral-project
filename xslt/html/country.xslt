<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" version="5.0" encoding="UTF-8" indent="yes"/>

    <xsl:template match="/">
        <html lang="en">
            <head>
                <meta charset="utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <title>
                    Welcome to <xsl:value-of select="country/header/country_name"/>!
                </title>
                <link rel="stylesheet" href="{resolve-uri('style/style.css')}"/>
            </head>
            <body>
                <div id="toc+flag">
                    <nav id="toc">
                        <h2 id="toc-h2">Table of contents</h2>
                        <ol>
                            <xsl:apply-templates select="country/section" mode="toc"/>
                        </ol>
                    </nav>
                    <div id="flag">
                        <xsl:apply-templates select="country/header/flag"/>
                    </div>
                </div>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="header">
        <div id="top">
            <h1>
                <xsl:apply-templates select="country_name"/>
            </h1>
            <a href="index.html">Back To Countries List</a>
        </div>
    </xsl:template>

    <xsl:template match="section" mode="toc">
        <li>
            <a href="#{generate-id()}">
                <xsl:value-of select="@title"/>
            </a>
        </li>
    </xsl:template>

    <xsl:template match="section" mode="#default">
        <div class="section">
            <h2 id="{generate-id()}">
                <xsl:value-of select="@title"/>
            </h2>
            <div class="subsection">
                <xsl:apply-templates/>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="subsection">
        <h3>
            <xsl:value-of select="@title"/>
        </h3>
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="text_block">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xsl:template match="items">
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>

    <xsl:template match="item">
        <li>
            <xsl:value-of select="@title"/>:
            <xsl:value-of select="text()[normalize-space()]"/>
        </li>
    </xsl:template>

    <xsl:template match="item[not(@title)]">
        <li>
            <xsl:value-of select="text()[normalize-space()]"/>
        </li>
    </xsl:template>

    <xsl:template match="sol_item">
        <xsl:value-of select="@title"/>:
        <xsl:value-of select="text()[normalize-space()]"/>
    </xsl:template>

    <xsl:template match="images">
        <div class="images">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="map">
        <img src="{@src}" alt="Map of {country_name}" title="Map of {country_name}"/>
    </xsl:template>

    <xsl:template match="location">
        <img src="{@src}" alt="Location of {country_name}" title="Location of {country_name}"/>
    </xsl:template>

    <xsl:template match="flag">
        <img src="{@src}" alt="Flag of {country_name}" title="Flag of {country_name}"/>
    </xsl:template>

</xsl:stylesheet>