<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version="2.0">

    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="page" font-family="sans-serif"
                                       page-height="297mm" page-width="210mm"
                                       margin-top="2cm" margin-bottom="0.5cm"
                                       margin-left="2cm" margin-right="2cm">
                    <fo:region-body margin-top="10mm" margin-bottom="10mm"/>
                    <fo:region-before extent="2cm"/>
                    <fo:region-after extent="1cm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>

            <fo:page-sequence master-reference="page">
                <fo:static-content flow-name="xsl-region-before">
                    <fo:block border-after-style="solid" border-after-width="1px" text-align-last="justify">
                        <fo:inline font-family="sans-serif" font-weight="bold">
                            <xsl:value-of select="country/header/country_name"/>
                        </fo:inline>
                        <fo:leader leader-pattern="space"/>
                        BI-XML - Semestral project
                    </fo:block>
                </fo:static-content>

                <fo:static-content flow-name="xsl-region-after">
                    <fo:block text-align="center">
                        -
                        <fo:page-number/>
                        -
                    </fo:block>
                </fo:static-content>

                <fo:flow flow-name="xsl-region-body">
                    <xsl:apply-templates/>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

    <xsl:template match="header">
        <fo:block>
            <fo:block font-family="sans-serif" font-weight="bold" font-size="24pt" line-height="40pt" margin-top="16pt" margin-bottom="16pt">
                <xsl:value-of select="country_name"/>
            </fo:block>
            <fo:block>
                <fo:external-graphic
                        src="{flag/@src}"
                        content-height="scale-to-fit"
                        height="4.1cm"/>
            </fo:block>
        </fo:block>
    </xsl:template>

    <xsl:template match="section">
        <fo:block page-break-after="always">
            <fo:block font-size="16pt"
                      font-family="sans-serif"
                      font-weight="bold"
                      margin-top="24pt"
                      margin-bottom="24pt"
                      border-after-style="solid"
                      border-after-width="1px"
                      id="{generate-id(.)}">
                <xsl:value-of select="@title"/>
            </fo:block>
            <xsl:apply-templates select="subsection"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="subsection">
        <fo:block keep-together="1">
            <fo:block font-size="14pt"
                      font-family="sans-serif"
                      font-weight="bold"
                      margin-top="24pt"
                      margin-bottom="4pt">
                <xsl:value-of select="@title"/>
            </fo:block>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="text_block">
        <fo:block margin-top="12pt"
                  margin-bottom="4pt">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="items">
        <fo:block margin-left="8pt">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="item">
        <fo:block margin-top="2pt" margin-bottom="4pt">
            <xsl:value-of select="@title"/>:
            <xsl:value-of select="text()[normalize-space()]"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="item[not(@title)]">
        <fo:block margin-top="2pt" margin-bottom="4pt">
            <xsl:value-of select="text()[normalize-space()]"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="sol_item">
        <fo:block margin-top="2pt" margin-bottom="4pt">
            <xsl:value-of select="@title"/>:
            <xsl:value-of select="text()[normalize-space()]"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="images">
        <fo:block>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="map">
        <fo:external-graphic
                src="{@src}"
                content-height="scale-to-fit"
                height="4.1cm"/>
    </xsl:template>

    <xsl:template match="location">
        <fo:external-graphic
                src="{@src}"
                content-height="scale-to-fit"
                height="4.1cm"/>
    </xsl:template>

    <xsl:template match="flag">
        <fo:external-graphic
                src="{@src}"
                content-height="scale-to-fit"
                height="4.1cm"/>
    </xsl:template>

</xsl:stylesheet>