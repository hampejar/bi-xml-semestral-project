<?xml version="1.0" encoding="UTF-8"?>
<country iso-code="JP">

    <header>
        <country_name>Japan</country_name>
        <flag src="https://www.cia.gov/library/publications/the-world-factbook/graphics/flags/large/ja-lgflag.gif"/>
    </header>

    <section title="Introduction">
        <subsection title="Background">
            <text_block>
                In 1603, after decades of civil warfare, the Tokugawa shogunate (a military-led, dynastic government)
                ushered in a long period of relative political stability and isolation from foreign influence. For more
                than
                two centuries this policy enabled Japan to enjoy a flowering of its indigenous culture. Japan opened its
                ports after signing the Treaty of Kanagawa with the US in 1854 and began to intensively modernize and
                industrialize. During the late 19th and early 20th centuries, Japan became a regional power that was
                able to
                defeat the forces of both China and Russia. It occupied Korea, Formosa (Taiwan), and southern Sakhalin
                Island. In 1931-32 Japan occupied Manchuria, and in 1937 it launched a full-scale invasion of China.
                Japan
                attacked US forces in 1941 - triggering America's entry into World War II - and soon occupied much of
                East
                and Southeast Asia.
            </text_block>

            <text_block>
                After its defeat in World War II, Japan recovered to become an economic power and an
                ally of the US. While the emperor retains his throne as a symbol of national unity, elected politicians
                hold
                actual decision-making power. Following three decades of unprecedented growth, Japan's economy
                experienced a
                major slowdown starting in the 1990s, but the country remains an economic power. In March 2011, Japan's
                strongest-ever earthquake, and an accompanying tsunami, devastated the northeast part of Honshu island,
                killed thousands, and damaged several nuclear power plants. The catastrophe hobbled the country's
                economy
                and its energy infrastructure, and tested its ability to deal with humanitarian disasters. Prime
                Minister
                Shinzo ABE was reelected to office in December 2012, and has since embarked on ambitious economic and
                security reforms to improve Japan's economy and bolster the country's international standing.
            </text_block>
        </subsection>
    </section>

    <section title="Geography">
        <subsection title="Location">
            <text_block>
                Eastern Asia, island chain between the North Pacific Ocean and the Sea of Japan, east of the Korean
                Peninsula
            </text_block>
        </subsection>
        <subsection title="Geographic coordinates">
            <text_block>
                36 00 N, 138 00 E
            </text_block>
        </subsection>
        <subsection title="Map references">
            <images>
                <location
                        src="https://www.cia.gov/library/publications/the-world-factbook/attachments/locator-maps/JA-locator-map.gif"/>
                <map src="https://www.cia.gov/library/publications/the-world-factbook/graphics/maps/ja-map.gif"/>
            </images>
        </subsection>
        <subsection title="Area">
            <items>
                <item title="total">377,915 sq km</item>
                <item title="land">364,485 sq km</item>
                <item title="water">13,430 sq km</item>
                <item title="country comparison to the world">63</item>
            </items>
        </subsection>
        <subsection title="Area - comparative">
            <text_block>
                slightly smaller than California
            </text_block>
        </subsection>
        <subsection title="Land boundaries">
            <items>
                <item title="total">0 km</item>
                <item title="border countries (0)">no country</item>
            </items>
        </subsection>
        <subsection title="Coastline">
            <text_block>
                29,751 km
            </text_block>
        </subsection>
        <subsection title="Maritime claims">
            <items>
                <item title="territorial sea">12 nm; between 3 nm and 12 nm in the international straits - La Perouse or
                    Soya, Tsugaru, Osumi, and Eastern and Western Channels of the Korea or Tsushima Strait
                </item>
                <item title="exclusive economic zone">200 nm</item>
            </items>
        </subsection>
        <subsection title="Climate">
            <text_block>
                varies from tropical in south to cool temperate in north
            </text_block>
        </subsection>
        <subsection title="Terrain">
            <text_block>
                mostly rugged and mountainous
            </text_block>
        </subsection>
        <subsection title="Elevation">
            <items>
                <item title="mean elevation">438 m</item>
                <item title="elevation extremes">
                    <sub_item title="lowest point">Hachiro-gata -4 m</sub_item>
                    <sub_item title="highest point">Mount Fuji 3,776 m</sub_item>
                </item>
            </items>
        </subsection>
        <subsection title="Natural resources">
            <text_block>
                negligible mineral resources, fish, note, with virtually no natural energy resources, Japan is the
                world's
                largest importer of coal and liquefied natural gas, as well as the second largest importer of oil
            </text_block>
        </subsection>
        <subsection title="Land use">
            <items>
                <item title="agricultural land">12.5% (2011 est.)
                    <sub_item title="arable land">arable land: 11.7% (2011 est.) / permanent crops: 0.8% (2011 est.) /
                        permanent
                        pasture: 0% (2011 est.)
                    </sub_item>
                </item>
                <item title="forest">68.5% (2011 est.)</item>
                <item title="other">19% (2011 est.)</item>
            </items>
        </subsection>
        <subsection title="Irrigated land">
            <text_block>
                24,690 sq km (2012)
            </text_block>
        </subsection>
        <subsection title="Natural hazards">
            <text_block>
                many dormant and some active volcanoes; about 1,500 seismic occurrences (mostly tremors but occasional
                severe earthquakes) every year; tsunamis; typhoons
            </text_block>
        </subsection>
        <subsection title="Environment - current issues">
            <text_block>
                air pollution from power plant emissions results in acid rain; acidification of lakes and reservoirs
                degrading water quality and threatening aquatic life; Japan is one of the largest consumers of fish and
                tropical timber, contributing to the depletion of these resources in Asia and elsewhere; following the
                2011
                Fukushima nuclear disaster, Japan originally planned to phase out nuclear power, but it has now
                implemented
                a new policy of seeking to restart nuclear power plants that meet strict new safety standards; waste
                management is an ongoing issue; Japanese municipal facilities used to burn high volumes of trash, but
                air
                pollution issues forced the government to adopt an aggressive recycling policy
            </text_block>
        </subsection>
        <subsection title="Environment - international agreements">
            <items>
                <item title="party to">Antarctic-Environmental Protocol, Antarctic-Marine Living Resources, Antarctic
                    Seals,
                    Antarctic Treaty, Biodiversity, Climate Change, Climate Change-Kyoto Protocol, Desertification,
                    Endangered Species, Environmental Modification, Hazardous Wastes, Law of the Sea, Marine Dumping,
                    Ozone
                    Layer Protection, Ship Pollution, Tropical Timber 83, Tropical Timber 94, Wetlands, Whaling
                </item>
                <item title="signed, but not ratified">none of the selected agreements</item>
            </items>
        </subsection>
        <subsection title="Geography - note">
            <items>
                <item title="note 1">strategic location in northeast Asia; composed of four main islands - from north:
                    Hokkaido, Honshu (the largest and most populous), Shikoku, and Kyushu (the "Home Islands") - and
                    6,848
                    smaller islands and islets
                </item>
                <item title="note 2">Japan annually records the most earthquakes in the world; it is one of the
                    countries
                    along the Ring of Fire, a belt of active volcanoes and earthquake epicenters bordering the Pacific
                    Ocean; up to 90% of the world's earthquakes and some 75% of the world's volcanoes occur within the
                    Ring
                    of Fire
                </item>
            </items>
        </subsection>
    </section>

    <section title="People and Society">
        <subsection title="Population">
            <text_block>
                126,168,156 (July 2018 est.)
            </text_block>
            <sol_item title="country comparison to the world">10</sol_item>
        </subsection>
        <subsection title="Nationality">
            <items>
                <item title="noun">Japanese (singular and plural)</item>
                <item title="adjective">Japanese</item>
            </items>
        </subsection>
        <subsection title="Ethnic groups">
            <text_block>
                Japanese 98.1%, Chinese 0.5%, Korean 0.4%, other 1% (includes Filipino, Vietnamese, and Brazilian) (2016
                est.)
            </text_block>
        </subsection>
        <subsection title="Languages">
            <text_block>
                Japanese
            </text_block>
        </subsection>
        <subsection title="Religions">
            <text_block>
                Shintoism 70.4%, Buddhism 69.8%, Christianity 1.5%, other 6.9% (2015 est.)
            </text_block>
        </subsection>
        <subsection title="Age structure">
            <items>
                <item title="0-14 years">12.71% (male 8,251,336 /female 7,787,234)</item>
                <item title="15-24 years">9.63% (male 6,397,995 /female 5,746,140)</item>
                <item title="25-54 years">37.28% (male 23,246,562 /female 23,784,273)</item>
                <item title="55-64 years">12.01% (male 7,588,597 /female 7,563,245)</item>
                <item title="65 years and over">28.38% (male 15,655,860 /female 20,146,914) (2018 est.)</item>
            </items>
        </subsection>
        <subsection title="Median age">
            <items>
                <item title="total">47.7 years</item>
                <item title="male">46.4 years</item>
                <item title="female">49.2 years (2018 est.)</item>
                <item title="country comparison to the world">2</item>
            </items>
        </subsection>
        <subsection title="Population growth rate">
            <text_block>
                -0.24% (2018 est.)
            </text_block>
            <sol_item title="country comparison to the world">212</sol_item>
        </subsection>

        <subsection title="Net migration rate">
            <text_block>
                0 migrant(s)/1,000 population (2018 est.)
            </text_block>
            <sol_item title="country comparison to the world">87</sol_item>
        </subsection>
        <subsection title="Urbanization">
            <items>
                <item title="urban population">91.6% of total population (2018)</item>
                <item title="rate of urbanization">-0.14% annual rate of change (2015-20 est.)</item>
            </items>
        </subsection>
        <subsection title="Major urban areas - population">
            <text_block>
                37.468 million TOKYO (capital), 19.281 million Osaka, 9.507 million Nagoya, 5.551 million
                Kitakyushu-Fukuoka, 2.899 million Shizuoka-Hamamatsu, 2.665 million Sapporo (2018)
            </text_block>
        </subsection>
    </section>

    <section title="Government">
        <subsection title="Country name">
            <items>
                <item title="conventional long form">none</item>
                <item title="conventional short form">Japan</item>
                <item title="etymology">the English word for Japan comes via the Chinese name for the country "Cipangu";
                    both Nihon and Nippon mean "where the sun originates" and are frequently translated as "Land of the
                    Rising Sun"
                </item>
            </items>
        </subsection>
        <subsection title="Government type">
            <text_block>
                parliamentary constitutional monarchy
            </text_block>
        </subsection>
        <subsection title="Capital">
            <items>
                <item title="name">Tokyo</item>
                <item title="geographic coordinates">35 41 N, 139 45 E</item>
                <item title="time difference">UTC+9 (14 hours ahead of Washington, DC, during Standard Time)</item>
            </items>
        </subsection>
        <subsection title="Administrative divisions">
            <text_block>
                47 prefectures; Aichi, Akita, Aomori, Chiba, Ehime, Fukui, Fukuoka, Fukushima, Gifu, Gunma, Hiroshima,
                Hokkaido, Hyogo, Ibaraki, Ishikawa, Iwate, Kagawa, Kagoshima, Kanagawa, Kochi, Kumamoto, Kyoto, Mie,
                Miyagi,
                Miyazaki, Nagano, Nagasaki, Nara, Niigata, Oita, Okayama, Okinawa, Osaka, Saga, Saitama, Shiga, Shimane,
                Shizuoka, Tochigi, Tokushima, Tokyo, Tottori, Toyama, Wakayama, Yamagata, Yamaguchi, Yamanashi
            </text_block>
        </subsection>
        <subsection title="Independence">
            <text_block>
                3 May 1947 (current constitution adopted as amendment to Meiji Constitution); notable earlier dates: 11
                February 660 B.C. (mythological date of the founding of the nation by Emperor JIMMU); 29 November 1890
                (Meiji Constitution provides for constitutional monarchy)
            </text_block>
        </subsection>
        <subsection title="National holiday">
            <text_block>
                Birthday of Emperor AKIHITO, 23 December (1933); note - celebrates the birthday of the current emperor
            </text_block>
        </subsection>
        <subsection title="Constitution">
            <text_block>
                previous 1890; latest approved 6 October 1946, adopted 3 November 1946, effective 3 May 1947
            </text_block>
        </subsection>
        <subsection title="Legal system">
            <text_block>
                civil law system based on German model; system also reflects Anglo-American influence and Japanese
                traditions; judicial review of legislative acts in the Supreme Court
            </text_block>
        </subsection>
        <subsection title="International law organization participation">
            <text_block>
                accepts compulsory ICJ jurisdiction with reservations; accepts ICCt jurisdiction
            </text_block>
        </subsection>
        <subsection title="Citizenship">
            <items>
                <item title="citizenship by birth">no</item>
                <item title="citizenship by descent only">at least one parent must be a citizen of Japan</item>
                <item title="dual citizenship recognized">no</item>
                <item title="residency requirement for naturalization">5 years</item>
            </items>
        </subsection>
        <subsection title="Suffrage">
            <text_block>
                18 years of age; universal
            </text_block>
        </subsection>
        <subsection title="Executive branch">
            <items>
                <item title="chief of state">Emperor NARUHITO (since 1 May 2019); note - succeeds his father who
                    abdicated
                    on 30 April 2019
                </item>
                <item title="head of government">Prime Minister Shinzo ABE (since 26 December 2012); Deputy Prime
                    Minister
                    Taro ASO (since 26 December 2012)
                </item>
                <item title="cabinet">Cabinet appointed by the prime minister</item>
                <item title="elections/appointments">tthe monarchy is hereditary; the leader of the majority party or
                    majority coalition in the House of Representatives usually becomes prime minister
                </item>
            </items>
        </subsection>
        <subsection title="Legislative branch">
            <items>
                <item title="description">bicameral Diet or Kokkai consists of: House of Councillors or Sangi-in (242
                    seats;
                    146 members directly elected in multi-seat districts by simple majority vote and 96 directly elected
                    in
                    a single national constituency by proportional representation vote; members serve 6-year terms with
                    half
                    the membership renewed every 3 years); and House of Representatives or Shugi-in (465 seats; 289
                    members
                    directly elected in single-seat districts by simple majority vote and 176 directly elected in
                    multi-seat
                    districts by party-list proportional representation vote; members serve 4-year terms)
                </item>
                <item title="elections/appointments">House of Councillors - last held on 10 July 2016 (next to be held
                    in
                    July 2019); House of Representatives - last held on 22 October 2017 (next to be held by 21 October
                    2021)
                </item>
            </items>
        </subsection>
        <subsection title="Judicial branch">
            <items>
                <item title="highest resident court(s)">Supreme Court or Saiko saibansho (consists of the chief justice
                    and
                    14 associate justices); note - the Supreme Court has jurisdiction in constitutional issues
                </item>
                <item title="judge selection and term of office">Supreme Court chief justice designated by the Cabinet
                    and
                    appointed by the monarch; associate justices appointed by the Cabinet and confirmed by the monarch;
                    all
                    justices are reviewed in a popular referendum at the first general election of the House of
                    Representatives following each judge's appointment and every 10 years afterward
                </item>
                <item title="subordinate courts">8 High Courts (Koto-saiban-sho), each with a Family Court
                    (Katei-saiban-sho); 50 District Courts (Chiho saibansho), with 203 additional branches; 438 Summary
                    Courts (Kani saibansho)
                </item>
            </items>
        </subsection>
    </section>

    <section title="Economy">
        <subsection title="Economy - overview">
            <text_block>
                Over the past 70 years, government-industry cooperation, a strong work ethic, mastery of high
                technology,
                and a comparatively small defense allocation (slightly less than 1% of GDP) have helped Japan develop an
                advanced economy. Two notable characteristics of the post-World War II economy were the close
                interlocking
                structures of manufacturers, suppliers, and distributors, known as keiretsu, and the guarantee of
                lifetime
                employment for a substantial portion of the urban labor force. Both features have significantly eroded
                under
                the dual pressures of global competition and domestic demographic change.
            </text_block>

            <text_block>
                Measured on a purchasing power parity basis that adjusts for price differences, Japan in 2017 stood as
                the
                fourth-largest economy in the world after first-place China, which surpassed Japan in 2001, and
                third-place
                India, which edged out Japan in 2012. For three postwar decades, overall real economic growth was
                impressive
                - averaging 10% in the 1960s, 5% in the 1970s, and 4% in the 1980s. Growth slowed markedly in the 1990s,
                averaging just 1.7%, largely because of the aftereffects of inefficient investment and the collapse of
                an
                asset price bubble in the late 1980s, which resulted in several years of economic stagnation as firms
                sought
                to reduce excess debt, capital, and labor. Modest economic growth continued after 2000, but the economy
                has
                fallen into recession four times since 2008.
            </text_block>

            <text_block>
                Japan enjoyed an uptick in growth since 2013, supported by Prime Minister Shinzo ABE’s "Three Arrows"
                economic revitalization agenda - dubbed "Abenomics" - of monetary easing, "flexible" fiscal policy, and
                structural reform. Led by the Bank of Japan’s aggressive monetary easing, Japan is making modest
                progress in
                ending deflation, but demographic decline – a low birthrate and an aging, shrinking population – poses a
                major long-term challenge for the economy. The government currently faces the quandary of balancing its
                efforts to stimulate growth and institute economic reforms with the need to address its sizable public
                debt,
                which stands at 235% of GDP. To help raise government revenue, Japan adopted legislation in 2012 to
                gradually raise the consumption tax rate. However, the first such increase, in April 2014, led to a
                sharp
                contraction, so Prime Minister ABE has twice postponed the next increase, which is now scheduled for
                October
                2019. Structural reforms to unlock productivity are seen as central to strengthening the economy in the
                long-run.
            </text_block>

            <text_block>
                Scarce in critical natural resources, Japan has long been dependent on imported energy and raw
                materials.
                After the complete shutdown of Japan’s nuclear reactors following the earthquake and tsunami disaster in
                2011, Japan's industrial sector has become even more dependent than before on imported fossil fuels.
                However, ABE’s government is seeking to restart nuclear power plants that meet strict new safety
                standards
                and is emphasizing nuclear energy’s importance as a base-load electricity source. In August 2015, Japan
                successfully restarted one nuclear reactor at the Sendai Nuclear Power Plant in Kagoshima prefecture,
                and
                several other reactors around the country have since resumed operations; however, opposition from local
                governments has delayed several more restarts that remain pending. Reforms of the electricity and gas
                sectors, including full liberalization of Japan’s energy market in April 2016 and gas market in April
                2017,
                constitute an important part of Prime Minister Abe’s economic program.
            </text_block>

            <text_block>
                Under the Abe Administration, Japan’s government sought to open the country’s economy to greater foreign
                competition and create new export opportunities for Japanese businesses, including by joining 11 trading
                partners in the Trans-Pacific Partnership (TPP). Japan became the first country to ratify the TPP in
                December 2016, but the United States signaled its withdrawal from the agreement in January 2017. In
                November
                2017 the remaining 11 countries agreed on the core elements of a modified agreement, which they renamed
                the
                Comprehensive and Progressive Agreement for Trans-Pacific Partnership (CPTPP). Japan also reached
                agreement
                with the European Union on an Economic Partnership Agreement in July 2017, and is likely seek to ratify
                both
                agreements in the Diet this year.
            </text_block>
        </subsection>
        <subsection title="GDP (purchasing power parity)">
            <items>
                <item>$5.443 trillion (2017 est.)</item>
                <item>$5.35 trillion (2016 est.)</item>
                <item>$5.299 trillion (2015 est.)</item>
                <item title="note">data are in 2017 dollars</item>
                <item title="country comparison to the world">4</item>
            </items>
        </subsection>
        <subsection title="GDP (official exchange rate)">
            <text_block>
                $4.873 trillion (2017 est.)
            </text_block>
        </subsection>
        <subsection title="GDP - real growth rate">
            <items>
                <item>1.7% (2017 est.)</item>
                <item>1% (2016 est.)</item>
                <item>1.4% (2015 est.)</item>
                <item title="country comparison to the world">164</item>
            </items>
        </subsection>
        <subsection title="GDP - per capita (PPP)">
            <items>
                <item>$42,900 (2017 est.)</item>
                <item>$42,100 (2016 est.)</item>
                <item>$41,700 (2015 est.)</item>
                <item title="note">data are in 2017 dollars</item>
                <item title="country comparison to the world">42</item>
            </items>
        </subsection>
        <subsection title="Gross national saving">
            <items>
                <item>28% of GDP (2017 est.)</item>
                <item>27.5% of GDP (2016 est.)</item>
                <item>27.1% of GDP (2015 est.)</item>
                <item title="country comparison to the world">88</item>
            </items>
        </subsection>
        <subsection title="GDP - composition, by end use">
            <items>
                <item title="household consumption">55.5% (2017 est.)</item>
                <item title="government consumption">19.6% (2017 est.)</item>
                <item title="investment in fixed capital">24% (2017 est.)</item>
                <item title="investment in inventories">0% (2017 est.)</item>
                <item title="exports of goods and services">17.7% (2017 est.)</item>
                <item title="imports of goods and services">-16.8% (2017 est.)</item>
            </items>
        </subsection>

        <subsection title="Agriculture - products">
            <text_block>
                vegetables, rice, fish, poultry, fruit, dairy products, pork, beef, flowers, potatoes/taros/yams,
                sugarcane,
                tea, legumes, wheat and barley
            </text_block>
        </subsection>
        <subsection title="Industries">
            <text_block>
                among world's largest and most technologically advanced producers of motor vehicles, electronic
                equipment,
                machine tools, steel and nonferrous metals, ships, chemicals, textiles, processed foods
            </text_block>
        </subsection>
        <subsection title="Industrial production growth rate">
            <text_block>
                1.4% (2017 est.)
            </text_block>
            <sol_item title="country comparison to the world">145</sol_item>
        </subsection>
        <subsection title="Labor force">
            <text_block>
                65.01 million (2017 est.)
            </text_block>
            <sol_item title="country comparison to the world">8</sol_item>
        </subsection>

        <subsection title="Unemployment rate">
            <items>
                <item>2.9% (2017 est.)</item>
                <item>3.1% (2016 est.)</item>
                <item title="country comparison to the world">34</item>
            </items>
        </subsection>
    </section>

    <section title="Energy">
        <subsection title="Electricity access">
            <sol_item title="electrification - total population">100%</sol_item>
        </subsection>
        <subsection title="Electricity - production">
            <text_block>
                989.3 billion kWh (2016 est.)
            </text_block>
            <sol_item title="country comparison to the world">5</sol_item>
        </subsection>
        <subsection title="Electricity - consumption">
            <text_block>
                943.7 billion kWh (2016 est.)
            </text_block>
            <sol_item title="country comparison to the world">4</sol_item>
        </subsection>
        <subsection title="Electricity - exports">
            <text_block>
                0 kWh (2016 est.)
            </text_block>
            <sol_item title="country comparison to the world">152</sol_item>
        </subsection>
        <subsection title="Electricity - imports">
            <text_block>
                0 kWh (2016 est.)
            </text_block>
            <sol_item title="country comparison to the world">163</sol_item>
        </subsection>
        <subsection title="Electricity - installed generating capacity">
            <text_block>
                295.9 million kW (2016 est.)
            </text_block>
            <sol_item title="country comparison to the world">4</sol_item>
        </subsection>
    </section>

    <section title="Communications">
        <subsection title="Telephones - fixed lines">
            <items>
                <item title="total subscriptions">63,941,094 (2017 est.)</item>
                <item title="subscriptions per 100 inhabitants">51 (2017 est.)</item>
                <item title="country comparison to the world">3</item>
            </items>
        </subsection>
        <subsection title="Telephones - mobile cellular">
            <items>
                <item title="total">170,128,499 (2017 est.)</item>
                <item title="subscriptions per 100 inhabitants">135 (2017 est.)</item>
                <item title="country comparison to the world">7</item>
            </items>
        </subsection>
        <subsection title="Telephone system">
            <items>
                <item title="general assessment">excellent domestic and international service; Japan has exceedingly
                    high
                    mobile, mobile broadband and fixed broadband penetration; one of Japan's largest e-commerce
                    companies
                    planning to build its own nationwide 4G mobile network; Japan govt. to open up to new mobile network
                    operators (MNO) for 5G services to be commercially available in 2020; Japan's first Cellular
                    Vehicle-to-Everything (C-V2X) commencing during 2018 (2017)
                </item>
                <item title="domestic">high level of modern technology and excellent service of every kind; 51 per 100
                    for
                    fixed-line and 135 per 100 for mobile-cellular subscriptions (2017)
                </item>
                <item title="international">country code - 81; numerous submarine cables provide links throughout Asia,
                    Australia, the Middle East, Europe, and US; satellite earth stations - 7 Intelsat (Pacific and
                    Indian
                    Oceans), 1 Intersputnik (Indian Ocean region), 2 Inmarsat (Pacific and Indian Ocean regions), and 8
                    SkyPerfect JSAT (2017)
                </item>
            </items>
        </subsection>
        <subsection title="Broadcast media">
            <text_block>
                a mixture of public and commercial broadcast TV and radio stations; 6 national terrestrial TV networks
                including 1 public broadcaster; the large number of radio and TV stations available provide a wide range
                of
                choices; satellite and cable services provide access to international channels (2012)
            </text_block>
        </subsection>
        <subsection title="Internet country code">
            <text_block>
                .jp
            </text_block>
        </subsection>
        <subsection title="Internet users">
            <items>
                <item title="total">116,565,962 (July 2016 est.)</item>
                <item title="percent of population">92% (July 2016 est.)</item>
                <item title="country comparison to the world">5</item>
            </items>
        </subsection>
    </section>

    <section title="Transportation">
        <subsection title="National air transport system">
            <items>
                <item title="number of registered air carriers">23 (2015)</item>
                <item title="inventory of registered aircraft operated by air carriers">627 (2015)</item>
                <item title="annual passenger traffic on registered air carriers">113.762 million (2015)</item>
                <item title="annual freight traffic on registered air carriers">8,868,745,000 mt-km (2015)</item>
            </items>
        </subsection>
        <subsection title="Civil aircraft registration country code prefix">
            <text_block>
                JA (2016)
            </text_block>
        </subsection>
        <subsection title="Airports">
            <text_block>
                175 (2013)
            </text_block>
            <sol_item title="country comparison to the world">33</sol_item>
        </subsection>
        <subsection title="Airports - with paved runways">
            <items>
                <item title="total">142 (2017)</item>
                <item title="over 3,047 m">6 (2017)</item>
            </items>
        </subsection>
        <subsection title="Heliports">
            <text_block>
                16 (2013)
            </text_block>
        </subsection>
        <subsection title="Pipelines">
            <text_block>
                4456 km gas, 174 km oil, 104 km oil/gas/water (2013)
            </text_block>
        </subsection>
        <subsection title="Roadways">
            <items>
                <item title="total">1,218,772 km (2015)</item>
                <item title="paved">992,835 km (includes 8,428 km of expressways) (2015)</item>
                <item title="unpaved">225,937 km (2015)</item>
                <item title="country comparison to the world">5</item>
            </items>
        </subsection>
        <subsection title="Waterways">
            <text_block>
                1,770 km (seagoing vessels use inland seas) (2010)
            </text_block>
            <sol_item title="country comparison to the world">44</sol_item>
        </subsection>
    </section>

    <section title="Military and Security">
        <subsection title="Military expenditures">
            <items>
                <item>0.93% of GDP (2016)</item>
                <item>0.94% of GDP (2015)</item>
                <item>0.96% of GDP (2014)</item>
                <item>0.95% of GDP (2013)</item>
                <item>0.97% of GDP (2012)</item>
                <item title="country comparison to the world">119</item>
            </items>
        </subsection>
        <subsection title="Military branches">
            <text_block>
                Japanese Ministry of Defense (MOD): Ground Self-Defense Force (Rikujou Jieitai, GSDF), Maritime
                Self-Defense
                Force (Kaijou Jieitai, MSDF), Air Self-Defense Force (Koukuu Jieitai, ASDF) (2011)
            </text_block>
        </subsection>
        <subsection title="Military service age and obligation">
            <text_block>
                18 years of age for voluntary military service; no conscription; mandatory retirement at age 53 for
                senior
                enlisted personnel and at 62 years for senior service officers (2012)
            </text_block>
        </subsection>
    </section>

    <section title="Transnational Issues">
        <subsection title="Disputes - international">
            <text_block>
                the sovereignty dispute over the islands of Etorofu, Kunashiri, and Shikotan, and the Habomai group,
                known
                in Japan as the "Northern Territories" and in Russia as the "Southern Kuril Islands," occupied by the
                Soviet
                Union in 1945, now administered by Russia and claimed by Japan, remains the primary sticking point to
                signing a peace treaty formally ending World War II hostilities; Japan and South Korea claim Liancourt
                Rocks
                (Take-shima/Tok-do) occupied by South Korea since 1954; the Japanese-administered Senkaku Islands are
                also
                claimed by China and Taiwan
            </text_block>
        </subsection>
        <subsection title="Refugees and internally displaced persons">
            <sol_item title="stateless persons">the sovereignty dispute over the islands of Etorofu, Kunashiri, and
                Shikotan, and the Habomai group, known in Japan as the "Northern Territories" and in Russia as the
                "Southern Kuril Islands," occupied by the Soviet Union in 1945, now administered by Russia and claimed
                by Japan, remains the primary sticking point to signing a peace treaty formally ending World War II
                hostilities; Japan and South Korea claim Liancourt Rocks (Take-shima/Tok-do) occupied by South Korea
                since 1954; the Japanese-administered Senkaku Islands are also claimed by China and Taiwan
            </sol_item>
        </subsection>
    </section>
</country>