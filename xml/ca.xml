<?xml version="1.0" encoding="UTF-8"?>
<country iso-code="CA">

    <header>
        <country_name>Canada</country_name>
        <flag src="https://www.cia.gov/library/publications/the-world-factbook/graphics/flags/large/ca-lgflag.gif"/>
    </header>

    <section title="Introduction">
        <subsection title="Background">
            <text_block>
                A land of vast distances and rich natural resources, Canada became a self-governing dominion in 1867,
                while retaining ties to the British crown. Canada repatriated its constitution from the UK in 1982,
                severing a final colonial tie. Economically and technologically, the nation has developed in parallel
                with the US, its neighbor to the south across the world's longest international border. Canada faces the
                political challenges of meeting public demands for quality improvements in health care, education,
                social services, and economic competitiveness, as well as responding to the particular concerns of
                predominantly francophone Quebec. Canada also aims to develop its diverse energy resources while
                maintaining its commitment to the environment.
            </text_block>
        </subsection>
    </section>

    <section title="Geography">
        <subsection title="Location">
            <text_block>
                Northern North America, bordering the North Atlantic Ocean on the east, North Pacific Ocean on the west,
                and
                the Arctic Ocean on the north, north of the conterminous US
            </text_block>
        </subsection>
        <subsection title="Geographic coordinates">
            <text_block>
                60 00 N, 95 00 W
            </text_block>
        </subsection>
        <subsection title="Map references">
            <images>
                <location
                        src="https://www.cia.gov/library/publications/the-world-factbook/attachments/locator-maps/CA-locator-map.gif"/>
                <map src="https://www.cia.gov/library/publications/the-world-factbook/graphics/maps/ca-map.gif"/>
            </images>
        </subsection>
        <subsection title="Area">
            <items>
                <item title="total">9,984,670 sq km</item>
                <item title="land">9,093,507 sq km</item>
                <item title="water">891,163 sq km</item>
                <item title="country comparison to the world">3</item>
            </items>
        </subsection>
        <subsection title="Area - comparative">
            <text_block>
                slightly larger than the US
            </text_block>
        </subsection>
        <subsection title="Land boundaries">
            <items>
                <item title="total">8,893 km</item>
                <item title="border countries (1)">US 8893 km (includes 2477 km with Alaska)</item>
            </items>
        </subsection>
        <subsection title="Coastline">
            <text_block>
                202,080 km
            </text_block>
        </subsection>
        <subsection title="Maritime claims">
            <items>
                <item title="territorial sea">12 nm</item>
                <item title="exclusive economic zone">200 nm</item>
            </items>
        </subsection>
        <subsection title="Climate">
            <text_block>
                varies from temperate in south to subarctic and arctic in north
            </text_block>
        </subsection>
        <subsection title="Terrain">
            <text_block>
                mostly plains with mountains in west, lowlands in southeast
            </text_block>
        </subsection>
        <subsection title="Elevation">
            <items>
                <item title="mean elevation">487 m</item>
                <item title="elevation extremes">
                    <sub_item title="lowest point">Atlantic Ocean 0 m</sub_item>
                    <sub_item title="highest point">Mount Logan 5,959 m</sub_item>
                </item>
            </items>
        </subsection>
        <subsection title="Natural resources">
            <text_block>
                bauxite, iron ore, nickel, zinc, copper, gold, lead, rare earth elements, molybdenum, potash, diamonds,
                silver, fish, timber, wildlife, coal, petroleum, natural gas, hydropower
            </text_block>
        </subsection>
        <subsection title="Land use">
            <items>
                <item title="agricultural land">6.8% (2011 est.)
                    <sub_item title="arable land">arable land: 4.7% (2011 est.) / permanent crops: 0.5% (2011 est.) /
                        permanent
                        pasture: 1.6% (2011 est.)
                    </sub_item>
                </item>
                <item title="forest">34.1% (2011 est.)</item>
                <item title="other">59.1% (2011 est.)</item>
            </items>
        </subsection>
        <subsection title="Irrigated land">
            <text_block>
                8,700 sq km (2012)
            </text_block>
        </subsection>
        <subsection title="Natural hazards">
            <text_block>
                continuous permafrost in north is a serious obstacle to development; cyclonic storms form east of the
                Rocky
                Mountains, a result of the mixing of air masses from the Arctic, Pacific, and North American interior,
                and
                produce most of the country's rain and snow east of the mountains
            </text_block>
        </subsection>
        <subsection title="Environment - current issues">
            <text_block>
                metal smelting, coal-burning utilities, and vehicle emissions impacting agricultural and forest
                productivity; air pollution and resulting acid rain severely affecting lakes and damaging forests; ocean
                waters becoming contaminated due to agricultural, industrial, mining, and forestry activities
            </text_block>
        </subsection>
        <subsection title="Environment - international agreements">
            <items>
                <item title="party to">Air Pollution, Air Pollution-Nitrogen Oxides, Air Pollution-Persistent Organic
                    Pollutants, Air Pollution-Sulfur 85, Air Pollution-Sulfur 94, Antarctic-Environmental Protocol,
                    Antarctic-Marine Living Resources, Antarctic Seals, Antarctic Treaty, Biodiversity, Climate Change,
                    Desertification, Endangered Species, Environmental Modification, Hazardous Wastes, Law of the Sea,
                    Marine Dumping, Ozone Layer Protection, Ship Pollution, Tropical Timber 83, Tropical Timber 94,
                    Wetlands
                </item>
                <item title="signed, but not ratified">Air Pollution-Volatile Organic Compounds, Marine Life
                    Conservation
                </item>
            </items>
        </subsection>
        <subsection title="Geography - note">
            <items>
                <item title="note 1">second-largest country in world (after Russia) and largest in the Americas;
                    strategic
                    location between Russia and US via north polar route; approximately 90% of the population is
                    concentrated within 160 km (100 mi) of the US border
                </item>
                <item title="note 2">Canada has more fresh water than any other country and almost 9% of Canadian
                    territory
                    is water; Canada has at least 2 million and possibly over 3 million lakes - that is more than all
                    other
                    countries combined
                </item>
            </items>
        </subsection>
    </section>

    <section title="People and Society">
        <subsection title="Population">
            <text_block>
                35,881,659 (July 2018 est.)
            </text_block>
            <sol_item title="country comparison to the world">38</sol_item>
        </subsection>
        <subsection title="Nationality">
            <items>
                <item title="noun">Canadian(s)</item>
                <item title="adjective">Canadian</item>
            </items>
        </subsection>
        <subsection title="Ethnic groups">
            <text_block>
                Canadian 32.3%, English 18.3%, Scottish 13.9%, French 13.6%, Irish 13.4%, German 9.6%, Chinese 5.1%,
                Italian
                4.6%, North American Indian 4.4%, East Indian 4%, other 51.6% (2016 est.)
            </text_block>
        </subsection>
        <subsection title="Languages">
            <text_block>
                English (official) 58.7%, French (official) 22%, Punjabi 1.4%, Italian 1.3%, Spanish 1.3%, German 1.3%,
                Cantonese 1.2%, Tagalog 1.2%, Arabic 1.1%, other 10.5% (2011 est.)
            </text_block>
        </subsection>
        <subsection title="Religions">
            <text_block>
                Catholic 39% (includes Roman Catholic 38.8%, other Catholic .2%), Protestant 20.3% (includes United
                Church
                6.1%, Anglican 5%, Baptist 1.9%, Lutheran 1.5%, Pentecostal 1.5%, Presbyterian 1.4%, other Protestant
                2.9%),
                Orthodox 1.6%, other Christian 6.3%, Muslim 3.2%, Hindu 1.5%, Sikh 1.4%, Buddhist 1.1%, Jewish 1%, other
                0.6%, none 23.9% (2011 est.)
            </text_block>
        </subsection>
        <subsection title="Age structure">
            <items>
                <item title="0-14 years">15.43% (male 2,839,236 /female 2,698,592)</item>
                <item title="15-24 years">11.62% (male 2,145,626 /female 2,023,369)</item>
                <item title="25-54 years">39.62% (male 7,215,261 /female 7,002,546)</item>
                <item title="55-64 years">14.24% (male 2,538,820 /female 2,570,709)</item>
                <item title="65 years and over">19.08% (male 3,055,560 /female 3,791,940) (2018 est.)</item>
            </items>
        </subsection>
        <subsection title="Median age">
            <items>
                <item title="total">42.4 years</item>
                <item title="male">41.1 years</item>
                <item title="female">43.7 years (2018 est.)</item>
                <item title="country comparison to the world">31</item>
            </items>
        </subsection>
        <subsection title="Population growth rate">
            <text_block>
                0.72% (2018 est.)
            </text_block>
            <sol_item title="country comparison to the world">139</sol_item>
        </subsection>

        <subsection title="Net migration rate">
            <text_block>
                5.7 migrant(s)/1,000 population (2018 est.)
            </text_block>
            <sol_item title="country comparison to the world">20</sol_item>
        </subsection>
        <subsection title="Urbanization">
            <items>
                <item title="urban population">81.4% of total population (2018)</item>
                <item title="rate of urbanization">0.97% annual rate of change (2015-20 est.)</item>
            </items>
        </subsection>
        <subsection title="Major urban areas - population">
            <text_block>
                6.082 million Toronto, 4.172 million Montreal, 2.531 million Vancouver, 1.477 million Calgary, 1.397
                million
                Edmonton, 1.363 million OTTAWA (capital) (2018)
            </text_block>
        </subsection>
    </section>

    <section title="Government">
        <subsection title="Country name">
            <items>
                <item title="conventional long form">none</item>
                <item title="conventional short form">Canada</item>
                <item title="etymology">the country name likely derives from the St. Lawrence Iroquoian word "kanata"
                    meaning village or settlement
                </item>
            </items>
        </subsection>
        <subsection title="Government type">
            <text_block>
                federal parliamentary democracy (Parliament of Canada) under a constitutional monarchy; a Commonwealth
                realm; federal and state authorities and responsibilities regulated in constitution
            </text_block>
        </subsection>
        <subsection title="Capital">
            <items>
                <item title="name">Ottawa</item>
                <item title="geographic coordinates">45 25 N, 75 42 W</item>
                <item title="time difference">UTC-5 (same time as Washington, DC, during Standard Time)</item>
            </items>
        </subsection>
        <subsection title="Administrative divisions">
            <text_block>
                10 provinces and 3 territories*; Alberta, British Columbia, Manitoba, New Brunswick, Newfoundland and
                Labrador, Northwest Territories*, Nova Scotia, Nunavut*, Ontario, Prince Edward Island, Quebec,
                Saskatchewan, Yukon*
            </text_block>
        </subsection>
        <subsection title="Independence">
            <text_block>
                1 July 1867 (union of British North American colonies); 11 December 1931 (recognized by UK per Statute
                of
                Westminster)
            </text_block>
        </subsection>
        <subsection title="National holiday">
            <text_block>
                Canada Day, 1 July (1867)
            </text_block>
        </subsection>
        <subsection title="Constitution">
            <text_block>
                consists of unwritten and written acts, customs, judicial decisions, and traditions dating from 1763;
                the
                written part of the constitution consists of the Constitution Act of 29 March 1867, which created a
                federation of four provinces, and the Constitution Act of 17 April 1982
            </text_block>
        </subsection>
        <subsection title="Legal system">
            <text_block>
                common law system except in Quebec, where civil law based on the French civil code prevails
            </text_block>
        </subsection>
        <subsection title="International law organization participation">
            <text_block>
                accepts compulsory ICJ jurisdiction with reservations; accepts ICCt jurisdiction
            </text_block>
        </subsection>
        <subsection title="Citizenship">
            <items>
                <item title="citizenship by birth">yes</item>
                <item title="citizenship by descent only">yes</item>
                <item title="dual citizenship recognized">yes</item>
                <item title="residency requirement for naturalization">minimum of 3 of last 5 years resident in Canada
                </item>
            </items>
        </subsection>
        <subsection title="Suffrage">
            <text_block>
                18 years of age; universal
            </text_block>
        </subsection>
        <subsection title="Executive branch">
            <items>
                <item title="chief of state">Queen ELIZABETH II (since 6 February 1952); represented by Governor General
                    Julie PAYETTE (since 2 October 2017)
                </item>
                <item title="head of government">Prime Minister Justin Pierre James TRUDEAU (Liberal Party) (since 4
                    November 2015)
                </item>
                <item title="cabinet">Federal Ministry chosen by the prime minister usually from among members of
                    his/her
                    own party sitting in Parliament
                </item>
                <item title="elections/appointments">the monarchy is hereditary; governor general appointed by the
                    monarch
                    on the advice of the prime minister for a 5-year term; following legislative elections, the leader
                    of
                    the majority party or majority coalition in the House of Commons generally designated prime minister
                    by
                    the governor general
                </item>
            </items>
        </subsection>
        <subsection title="Legislative branch">
            <items>
                <item title="description">bicameral Parliament or Parlement consists of: Senate or Senat (105 seats;
                    members
                    appointed by the governor general on the advice of the prime minister and can serve until age 75);
                    House
                    of Commons or Chambre des Communes (338 seats; members directly elected in single-seat
                    constituencies by
                    simple majority vote with terms up to 4 years)
                </item>
                <item title="elections/appointments">Senate - appointed; latest appointments in December 2018; House of
                    Commons - last held on 19 October 2015 (next to be held on 21 October 2019)
                </item>
            </items>
        </subsection>
        <subsection title="Judicial branch">
            <items>
                <item title="highest resident court(s)">Supreme Court of Canada (consists of the chief justice and 8
                    judges); note - in 1949, Canada abolished all appeals beyond its Supreme Court, which prior to that
                    time, were heard by the Judicial Committee of the Privy Council (in London)
                </item>
                <item title="judge selection and term of office">chief justice and judges appointed by the prime
                    minister in
                    council; all judges appointed for life with mandatory retirement at age 75
                </item>
                <item title="subordinate courts">federal level: Federal Court of Appeal; Federal Court; Tax Court;
                    federal
                    administrative tribunals; Courts Martial; provincial/territorial level: provincial superior,
                    appeals,
                    first instance, and specialized courts; note - in 1999, the Nunavut Court - a circuit court with the
                    power of a provincial superior court, as well as a territorial court - was established to serve
                    isolated
                    settlements
                </item>
            </items>
        </subsection>
    </section>

    <section title="Economy">
        <subsection title="Economy - overview">
            <text_block>
                Canada resembles the US in its market-oriented economic system, pattern of production, and high living
                standards. Since World War II, the impressive growth of the manufacturing, mining, and service sectors
                has
                transformed the nation from a largely rural economy into one primarily industrial and urban. Canada has
                a
                large oil and natural gas sector with the majority of crude oil production derived from oil sands in the
                western provinces, especially Alberta. Canada now ranks third in the world in proved oil reserves behind
                Venezuela and Saudi Arabia and is the world’s seventh-largest oil producer.
            </text_block>

            <text_block>
                TThe 1989 Canada-US Free Trade Agreement and the 1994 North American Free Trade Agreement (which
                includes
                Mexico) dramatically increased trade and economic integration between the US and Canada. Canada and the
                US
                enjoy the world’s most comprehensive bilateral trade and investment relationship, with goods and
                services
                trade totaling more than $680 billion in 2017, and two-way investment stocks of more than $800 billion.
                Over
                three-fourths of Canada’s merchandise exports are destined for the US each year. Canada is the largest
                foreign supplier of energy to the US, including oil, natural gas, and electric power, and a top source
                of US
                uranium imports.
            </text_block>

            <text_block>
                Given its abundant natural resources, highly skilled labor force, and modern capital stock, Canada
                enjoyed
                solid economic growth from 1993 through 2007. The global economic crisis of 2007-08 moved the Canadian
                economy into sharp recession by late 2008, and Ottawa posted its first fiscal deficit in 2009 after 12
                years
                of surplus. Canada's major banks emerged from the financial crisis of 2008-09 among the strongest in the
                world, owing to the financial sector's tradition of conservative lending practices and strong
                capitalization. Canada’s economy posted strong growth in 2017 at 3%, but most analysts are projecting
                Canada’s economic growth will drop back closer to 2% in 2018.
            </text_block>
        </subsection>
        <subsection title="GDP (purchasing power parity)">
            <items>
                <item>$1.774 trillion (2017 est.)</item>
                <item>$1.721 trillion (2016 est.)</item>
                <item>$1.697 trillion (2015 est.)</item>
                <item title="note">data are in 2017 dollars</item>
                <item title="country comparison to the world">17</item>
            </items>
        </subsection>
        <subsection title="GDP (official exchange rate)">
            <text_block>
                $1.653 trillion (2017 est.)
            </text_block>
        </subsection>
        <subsection title="GDP - real growth rate">
            <items>
                <item>3% (2017 est.)</item>
                <item>1.4% (2016 est.)</item>
                <item>1% (2015 est.)</item>
                <item title="country comparison to the world">112</item>
            </items>
        </subsection>
        <subsection title="GDP - per capita (PPP)">
            <items>
                <item>$48,400 (2017 est.)</item>
                <item>$47,500 (2016 est.)</item>
                <item>$47,400 (2015 est.)</item>
                <item title="note">data are in 2017 dollars</item>
                <item title="country comparison to the world">34</item>
            </items>
        </subsection>
        <subsection title="Gross national saving">
            <items>
                <item>20.8% of GDP (2017 est.)</item>
                <item>20% of GDP (2016 est.)</item>
                <item>20.5% of GDP (2015 est.)</item>
                <item title="country comparison to the world">90</item>
            </items>
        </subsection>
        <subsection title="GDP - composition, by end use">
            <items>
                <item title="household consumption">57.8% (2017 est.)</item>
                <item title="government consumption">20.8% (2017 est.)</item>
                <item title="investment in fixed capital">23% (2017 est.)</item>
                <item title="investment in inventories">0.7% (2017 est.)</item>
                <item title="exports of goods and services">30.9% (2017 est.)</item>
                <item title="imports of goods and services">-33.2% (2017 est.)</item>
            </items>
        </subsection>

        <subsection title="Agriculture - products">
            <text_block>
                wheat, barley, oilseed, tobacco, fruits, vegetables; dairy products; fish; forest products
            </text_block>
        </subsection>
        <subsection title="Industries">
            <text_block>
                transportation equipment, chemicals, processed and unprocessed minerals, food products, wood and paper
                products, fish products, petroleum, natural gas
            </text_block>
        </subsection>
        <subsection title="Industrial production growth rate">
            <text_block>
                4.9% (2017 est.)
            </text_block>
            <sol_item title="country comparison to the world">60</sol_item>
        </subsection>
        <subsection title="Labor force">
            <text_block>
                19.52 million (2017 est.)
            </text_block>
            <sol_item title="country comparison to the world">31</sol_item>
        </subsection>

        <subsection title="Unemployment rate">
            <items>
                <item>6.3% (2017 est.)</item>
                <item>7% (2016 est.)</item>
                <item title="country comparison to the world">94</item>
            </items>
        </subsection>
    </section>

    <section title="Energy">
        <subsection title="Electricity access">
            <sol_item title="electrification - total population">100%</sol_item>
        </subsection>
        <subsection title="Electricity - production">
            <text_block>
                649.6 billion kWh (2016 est.)
            </text_block>
            <sol_item title="country comparison to the world">6</sol_item>
        </subsection>
        <subsection title="Electricity - consumption">
            <text_block>
                522.2 billion kWh (2016 est.)
            </text_block>
            <sol_item title="country comparison to the world">7</sol_item>
        </subsection>
        <subsection title="Electricity - exports">
            <text_block>
                73.35 billion kWh (2016 est.)
            </text_block>
            <sol_item title="country comparison to the world">2</sol_item>
        </subsection>
        <subsection title="Electricity - imports">
            <text_block>
                2.682 billion kWh (2016 est.)
            </text_block>
            <sol_item title="country comparison to the world">52</sol_item>
        </subsection>
        <subsection title="Electricity - installed generating capacity">
            <text_block>
                143.5 million kW (2016 est.)
            </text_block>
            <sol_item title="country comparison to the world">8</sol_item>
        </subsection>
    </section>

    <section title="Communications">
        <subsection title="Telephones - fixed lines">
            <items>
                <item title="total subscriptions">14,700,854 (2017 est.)</item>
                <item title="subscriptions per 100 inhabitants">41 (2017 est.)</item>
                <item title="country comparison to the world">15</item>
            </items>
        </subsection>
        <subsection title="Telephones - mobile cellular">
            <items>
                <item title="total">31,458,600 (2017 est.)</item>
                <item title="subscriptions per 100 inhabitants">88 (2017 est.)</item>
                <item title="country comparison to the world">43</item>
            </items>
        </subsection>
        <subsection title="Telephone system">
            <items>
                <item title="general assessment">excellent service provided by modern technology; consumer demand for
                    mobile
                    data services have promted telecos to invest and advance LTE infrastructure, and further investment
                    in
                    5G; government policy has aided the extension of broadband to rural and regional areas, with the
                    result
                    that services are almost universally accessible
                </item>
                <item title="domestic">comparatively low mobile penetration provides further room for growth; domestic
                    satellite system with about 300 earth stations; 41 per 100 fixed-line; 88 per 100 mobile-cellular
                </item>
                <item title="international">country code - 1; submarine cables provide links to the US and Europe;
                    satellite
                    earth stations - 7 (5 Intelsat - 4 Atlantic Ocean and 1 Pacific Ocean, and 2 Intersputnik - Atlantic
                    Ocean region)
                </item>
            </items>
        </subsection>
        <subsection title="Broadcast media">
            <text_block>
                2 public TV broadcasting networks, 1 in English and 1 in French, each with a large number of network
                affiliates; several private-commercial networks also with multiple network affiliates; overall, about
                150 TV
                stations; multi-channel satellite and cable systems provide access to a wide range of stations including
                US
                stations; mix of public and commercial radio broadcasters with the Canadian Broadcasting Corporation
                (CBC),
                the public radio broadcaster, operating 4 radio networks, Radio Canada International, and radio services
                to
                indigenous populations in the north; roughly 1,119 licensed radio stations (2016)
            </text_block>
        </subsection>
        <subsection title="Internet country code">
            <text_block>
                .ca
            </text_block>
        </subsection>
        <subsection title="Internet users">
            <items>
                <item title="total">31,770,034 (July 2016 est.)</item>
                <item title="percent of population">89.8% (July 2016 est.)</item>
                <item title="country comparison to the world">21</item>
            </items>
        </subsection>
    </section>

    <section title="Transportation">
        <subsection title="National air transport system">
            <items>
                <item title="number of registered air carriers">51 (2015)</item>
                <item title="inventory of registered aircraft operated by air carriers">879 (2015)</item>
                <item title="annual passenger traffic on registered air carriers">80,228,301 (2015)</item>
                <item title="annual freight traffic on registered air carriers">2,074,830,881 mt-km (2015)</item>
            </items>
        </subsection>
        <subsection title="Civil aircraft registration country code prefix">
            <text_block>
                C (2016)
            </text_block>
        </subsection>
        <subsection title="Airports">
            <text_block>
                1,467 (2013)
            </text_block>
            <sol_item title="country comparison to the world">4</sol_item>
        </subsection>
        <subsection title="Airports - with paved runways">
            <items>
                <item title="total">523 (2017)</item>
                <item title="over 3,047 m">21 (2017)</item>
            </items>
        </subsection>
        <subsection title="Heliports">
            <text_block>
                26 (2013)
            </text_block>
        </subsection>
        <subsection title="Pipelines">
            <text_block>
                110000 km gas and liquid petroleum (2017)
            </text_block>
        </subsection>
        <subsection title="Roadways">
            <items>
                <item title="total">1,042,300 km (2011)</item>
                <item title="paved">415,600 km (includes 17,000 km of expressways) (2011)</item>
                <item title="unpaved">626,700 km (2011)</item>
                <item title="country comparison to the world">7</item>
            </items>
        </subsection>
        <subsection title="Waterways">
            <text_block>
                636 km (Saint Lawrence Seaway of 3,769 km, including the Saint Lawrence River of 3,058 km, shared with
                United States) (2011)
            </text_block>
            <sol_item title="country comparison to the world">77</sol_item>
        </subsection>
    </section>

    <section title="Military and Security">
        <subsection title="Military expenditures">
            <items>
                <item>1.29% of GDP (2017)</item>
                <item>1.16% of GDP (2016)</item>
                <item>1.2% of GDP (2015)</item>
                <item>1% of GDP (2014)</item>
                <item>1% of GDP (2013)</item>
                <item title="country comparison to the world">89</item>
            </items>
        </subsection>
        <subsection title="Military branches">
            <text_block>
                Canadian Forces: Canadian Army, Royal Canadian Navy, Royal Canadian Air Force, Canadian Joint Operations
                Command (2015)
            </text_block>
        </subsection>
        <subsection title="Military service age and obligation">
            <text_block>
                17 years of age for voluntary male and female military service (with parental consent); 16 years of age
                for
                Reserve and Military College applicants; Canadian citizenship or permanent residence status required;
                maximum 34 years of age; service obligation 3-9 years (2012)
            </text_block>
        </subsection>
    </section>

    <section title="Transnational Issues">
        <subsection title="Disputes - international">
            <text_block>
                managed maritime boundary disputes with the US at Dixon Entrance, Beaufort Sea, Strait of Juan de Fuca,
                and
                the Gulf of Maine, including the disputed Machias Seal Island and North Rock; Canada and the United
                States
                dispute how to divide the Beaufort Sea and the status of the Northwest Passage but continue to work
                cooperatively to survey the Arctic continental shelf; US works closely with Canada to intensify security
                measures for monitoring and controlling legal and illegal movement of people, transport, and commodities
                across the international border; sovereignty dispute with Denmark over Hans Island in the Kennedy
                Channel
                between Ellesmere Island and Greenland; commencing the collection of technical evidence for submission
                to
                the Commission on the Limits of the Continental Shelf in support of claims for continental shelf beyond
                200
                nm from its declared baselines in the Arctic, as stipulated in Article 76, paragraph 8, of the UN
                Convention
                on the Law of the Sea
            </text_block>
        </subsection>
        <subsection title="Refugees and internally displaced persons">
            <items>
                <item title="stateless persons">3,790 (2017)</item>
                <item title="refugees">8,049 (Colombia), 7,180 (China), 6,874 (Haiti) (2017); 5,705 (Venezuela)</item>
            </items>
        </subsection>
        <subsection title="Illicit drugs">
            <text_block>
                illicit producer of cannabis for the domestic drug market and export to US; use of hydroponics
                technology
                permits growers to plant large quantities of high-quality marijuana indoors; increasing ecstasy
                production,
                some of which is destined for the US; vulnerable to narcotics money laundering because of its mature
                financial services sector
            </text_block>
        </subsection>
    </section>
</country>
