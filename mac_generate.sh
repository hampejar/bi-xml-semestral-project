#!/bin/bash

echo ".. checking/creating project directories"

# Create output structure
mkdir -p out/html out/fo out/pdf

echo "^^ done!"
echo ".. clearing old output files"

# Clear output directories if not empty
rm -f ./out/html/* ./out/fo/* ./out/pdf/*

echo "^^ done!"
echo ".. generating HTML output for each country"

# Generate html for each country
saxon xml/as.xml xslt/html/country.xslt -o:out/html/as.html
saxon xml/ca.xml xslt/html/country.xslt -o:out/html/ca.html
saxon xml/jp.xml xslt/html/country.xslt -o:out/html/jp.html
saxon xml/nz.xml xslt/html/country.xslt -o:out/html/nz.html

echo "^^ done!"
echo ".. generating combined HTML output"

# Generate index.html
saxon xml/countries.xml xslt/html/countries.xslt -o:out/html/index.html

echo "^^ done!"
echo ".. generating XSL-FO output for each country"

# Generate XSL-FO for each country
saxon xml/as.xml xslt/pdf/country.xslt -o:out/fo/as.fo
saxon xml/ca.xml xslt/pdf/country.xslt -o:out/fo/ca.fo
saxon xml/jp.xml xslt/pdf/country.xslt -o:out/fo/jp.fo
saxon xml/nz.xml xslt/pdf/country.xslt -o:out/fo/nz.fo

echo "^^ done!"
echo ".. generating combined XSL-FO output"

# Generate XSL-FO for combined document
saxon xml/countries.xml xslt/pdf/countries.xslt -o:out/fo/combined.fo

echo "^^ done!"
echo ".. generating PDF output for each country"

# Generate PDF for each country
fop out/fo/as.fo out/pdf/as.pdf
fop out/fo/ca.fo out/pdf/ca.pdf
fop out/fo/jp.fo out/pdf/jp.pdf
fop out/fo/nz.fo out/pdf/nz.pdf

echo "^^done!"
echo ".. generating combined PDF output"

# Generate combined PDF
fop out/fo/combined.fo out/pdf/combined.pdf

echo "^^ done!"
echo "^^^ all done!"
