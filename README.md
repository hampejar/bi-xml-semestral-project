# Semestrální projekt BI-XML
* autor: Jaroslav Hampejs

## Adresářová struktura projektu

- rootovským adresářem je adresář bi-xml-semestral-project, který obsahuje:
	- skript [generate.sh](./generate.sh), který:
		- vygeneruje adresářovou strukturu pro výstupy
		- vygeneruje pro každou oblast její HTML dokument
		- vygeneruje jeden společný html dokument s odkazy na jednotlivé oblasti
		- vygeneruje pro každou oblast její XSL-FO soubor
		- vygeneruje jeden XSL-FO soubor pro oblasti sloučené do jednoho dokumentu
		- vygeneruje pro každou oblast její PDF dokument
		- vygeneruje jeden PDF dokument slučující všechny oblasti
	- skript [validate.sh](./validate.sh), který:
		- zvaliduje všechny XML dokumenty jednotlivých oblastí (valid XML)
		- totéž pomocí DTD souborů
		- totéž pomocí definic RelaxNG
	- adresář [/out](./out) a jeho podadresáře (fo, html, pdf), kam budou ukládány vygenerované XSL-FO, HTML a PDF soubory
	- adresář [/scheme](./scheme) obsahující DTD a RelaxNG definice
	- adresář [/xml](./xml) obsahující:
		- zdrojové XML dokumenty jednotlivých oblastí ([as.xml](./xml/as.xml), [ca.xml](./xml/ca.xml), [jp.xml](./xml/jp.xml), [nz.xml](./xml/nz.xml))
		- xml dokument sjednocující oblasti dohromady ([countries.xml](./xml/countries.xml))
	- adresář [/xslt](./xslt) obsahující:
		- podadresář [/html](./xslt/html), který obsahuje:
			- soubor [country.xslt](./xslt/html/country.xslt) pro transformaci XML dokumentů jednotlivých oblastí do HTML
			- soubor [countries.xslt](./xslt/html/countries.xslt) pro transformaci společného sjednocujícího xml dokumentu do HTML
			- adresář [/style](./xslt/html/style) obsahující soubor style.css pro nastylování výsledných HTML dokumentů
		- podadresář [/pdf](./xslt/pdf), který obsahuje:
			- soubor [country.xslt](./xslt/pdf/country.xslt) pro vygenerování XSL-FO souborů pro jednotlivé oblasti
			- soubor [countries.xslt](./xslt/pdf/countries.xslt) pro vygenerování sjednocujícího XSL-FO souboru pro všechny oblasti společně
	- soubor [README.md](./README.md) s informacemi k projektu

## Použitý software

- **xmllint** pro validování dokumentů
- **saxon** pro transformace xml dokumentů dle xslt stylesheetů
- **fop** pro generování PDF dokumentů z XSL-FO souborů
- **PhpStorm** jako vývojové prostředí pro xml soubory, xslt šablony, ...
